﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuegoCartas.Entidades
{
    public class Carta
    {
        public List<Atributo> Atributos { get; set; }
        public TipoCarta Tipo { get; set; }
        public string Nombre { get; set; }
        public string Codigo { get; set; }

        public enum TipoCarta
        {
            comun,
            roja,
            amarilla,
        }

        public Carta(string nombreCarta, string codigoCarta, TipoCarta tipoCarta)
        {
            if (string.IsNullOrEmpty(nombreCarta))
           
                throw new ArgumentNullException("El nombre no puede estar vacío");
                    
            if (string.IsNullOrEmpty(codigoCarta))
            
                throw new ArgumentNullException("El codigo no puede estar vacío");

            this.Nombre = nombreCarta;
            this.Codigo = codigoCarta;
            this.Tipo = tipoCarta;
            this.Atributos = new List<Atributo>();

        }
    }
}

