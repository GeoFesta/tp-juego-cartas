﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuegoCartas.Entidades
{
    public class Partida
    {
        public string Nombre { get; set; }
        public List<Jugador> Jugadores { get; set; }
        public Mazo MazoSeleccionado { get; set; }


        public Partida()
        {
            this.Jugadores = new List<Jugador>();

        }

        public Mazo MezclarCartas()
        {
            if (Jugadores.Count != 2)
            {
                throw new ArgumentNullException("Falta un jugador");
            }

            if (MazoSeleccionado == null)
            {
                throw new ArgumentNullException("Debe seleccionar un mazo");
            }
            List<Carta> listaMezclada = new List<Carta>();
            Random mezclar = new Random();
            while (this.MazoSeleccionado.Cartas.Count > 0)
            {
                int posicionDeCarta = mezclar.Next(0, this.MazoSeleccionado.Cartas.Count - 1);
                listaMezclada.Add(this.MazoSeleccionado.Cartas[posicionDeCarta]);
                this.MazoSeleccionado.Cartas.RemoveAt(posicionDeCarta);
            }
            this.MazoSeleccionado.Cartas = listaMezclada;
            return MazoSeleccionado;
        }

        public ListasCartas RepartirCartas()
        {
            MezclarCartas();
            
            for (int posicionCarta = 0; posicionCarta < MazoSeleccionado.Cartas.Count; posicionCarta++)
            {
                if (posicionCarta % 2 == 0)
                {
                    Jugadores[0].Cartas.Add(MazoSeleccionado.Cartas[posicionCarta]);
                }
                else
                {
                    Jugadores[1].Cartas.Add(MazoSeleccionado.Cartas[posicionCarta]);
                }
            }
            List<Carta> cartasJugador1 = Jugadores[0].Cartas;
            List<Carta> cartasJugador2 = Jugadores[1].Cartas;
            ListasCartas listasCartasDeJugadores = new ListasCartas(cartasJugador1, cartasJugador2);
            return listasCartasDeJugadores;
            
        }



        public class ListasCartas
        {
            public List<Carta> CartasJugador1 { get; set; }
            public List<Carta> CartasJugador2 { get; set; }

            public ListasCartas(List<Carta> cartas1, List<Carta> cartas2)
            {
                this.CartasJugador1 = cartas1;
                this.CartasJugador2 = cartas2;
            }
        }

        public Resultado CompararCartas(string atributo, string connectionIdJugadorQueCanto, string idCartaJugadorQueCanto)
        {
            Resultado resultado = new Resultado();
            Jugador jugadorQueCanto = this.Jugadores.Where(x => x.ConnectionId == connectionIdJugadorQueCanto).Single();
            Jugador jugadorQueEspera = this.Jugadores.Where(x => x.ConnectionId != connectionIdJugadorQueCanto).Single();
            //Carta cartaJugadorQueCanto = jugadorQueCanto.Cartas.Where(x=>x.Codigo== idCartaJugadorQueCanto).Single();
            Carta cartaJugadorQueCanto = jugadorQueCanto.Cartas[0];
            Carta cartaJugadorQueEspera = jugadorQueEspera.Cartas[0];

             if (cartaJugadorQueCanto.Tipo== Carta.TipoCarta.comun)
             {
                 var valorJugadorqueCanto = cartaJugadorQueCanto.Atributos.Where(x => x.Nombre == atributo).Single().Valor;

                 if (cartaJugadorQueEspera.Tipo == Carta.TipoCarta.comun)
                 {
                     var valorJugadorqueEspera = cartaJugadorQueEspera.Atributos.Where(x => x.Nombre == atributo).Single().Valor;

                     if (valorJugadorqueCanto>= valorJugadorqueEspera)
                     {
                        PasarCartaAtras(jugadorQueCanto, idCartaJugadorQueCanto);
                        RobarCarta(jugadorQueCanto, jugadorQueEspera);
                        resultado= new Resultado(jugadorQueCanto.ConnectionId,jugadorQueEspera.ConnectionId,tipoResultado.normal); //ganacanto
                     }
                     else
                     {
                        PasarCartaAtras(jugadorQueEspera, cartaJugadorQueEspera.Codigo);
                        RobarCarta(jugadorQueEspera,jugadorQueCanto);
                        resultado= new Resultado(jugadorQueEspera.ConnectionId, jugadorQueCanto.ConnectionId, tipoResultado.normal);
                     }

                 }
                 else
                 {
                    
                     switch (cartaJugadorQueEspera.Tipo)
                     {

                         case Carta.TipoCarta.roja:
                            RobarCarta(jugadorQueEspera,jugadorQueCanto);
                            RobarCarta(jugadorQueEspera, jugadorQueCanto);
                            EliminarCartaColor(jugadorQueEspera, cartaJugadorQueEspera);

                            resultado = new Resultado(jugadorQueEspera.ConnectionId, jugadorQueCanto.ConnectionId, tipoResultado.roja);                             
                             break;
                         case Carta.TipoCarta.amarilla:
                            RobarCarta(jugadorQueEspera, jugadorQueCanto);
                            EliminarCartaColor(jugadorQueEspera, cartaJugadorQueEspera);

                            resultado = new Resultado(jugadorQueEspera.ConnectionId, jugadorQueCanto.ConnectionId, tipoResultado.amarilla);
                            break;
                  
                     } 
                 }

             }
            else
            {
                if (cartaJugadorQueEspera.Tipo != Carta.TipoCarta.comun)
                {
                    if (cartaJugadorQueEspera.Tipo == Carta.TipoCarta.roja)
                    {
                        
                        RobarCarta(jugadorQueEspera, jugadorQueCanto);
                        RobarCarta(jugadorQueEspera, jugadorQueCanto);
                        EliminarCartaColor(jugadorQueEspera, cartaJugadorQueEspera);

                        resultado = new Resultado(jugadorQueEspera.ConnectionId, jugadorQueCanto.ConnectionId, tipoResultado.roja);
                    }
                    else
                    {
                        //si no es comun ni roja, el que espera tiene amarilla y el que canto tiene roja.
                        RobarCarta(jugadorQueCanto, jugadorQueEspera);
                        RobarCarta(jugadorQueCanto, jugadorQueEspera);
                        EliminarCartaColor(jugadorQueCanto, cartaJugadorQueCanto);

                        resultado = new Resultado(jugadorQueCanto.ConnectionId, jugadorQueEspera.ConnectionId, tipoResultado.roja);
                    }
                }
                else
                {
                    switch (cartaJugadorQueCanto.Tipo)  
                    {
                       //el jugador que espera tiene comun y el que canto tiene amarilla o roja
                        case Carta.TipoCarta.roja:
                            RobarCarta(jugadorQueCanto, jugadorQueEspera);
                            RobarCarta(jugadorQueCanto, jugadorQueEspera);
                            EliminarCartaColor(jugadorQueCanto, cartaJugadorQueCanto);

                            resultado = new Resultado(jugadorQueCanto.ConnectionId, jugadorQueEspera.ConnectionId, tipoResultado.roja);
                            break;
                        case Carta.TipoCarta.amarilla:

                            RobarCarta(jugadorQueCanto, jugadorQueEspera);
                            EliminarCartaColor(jugadorQueCanto, cartaJugadorQueCanto);

                            resultado = new Resultado(jugadorQueCanto.ConnectionId, jugadorQueEspera.ConnectionId, tipoResultado.amarilla);
                            break;

                    }
                }
            }

            if (jugadorQueEspera.Cartas.Count==0|| jugadorQueCanto.Cartas.Count == 0)
            {
                resultado.Finalizo = true;
            }
            else
            {
                resultado.Finalizo = false;
            }

            return resultado;

        }


        public enum tipoResultado
        {
            normal = 0,
            amarilla = 1,
            roja = 2,

        }

        public class Resultado
        {
            public tipoResultado TipoResultado { get; set; }
            public string IdConnectionGanador { get; set; }
            public string IdConnectionPerdedor { get; set; }
            public bool Finalizo { get; set; }

            public Resultado()
            {

            }
            public Resultado(string idGanador,string idPerdedor, tipoResultado tipoResultado)
            {
                this.TipoResultado = tipoResultado;
                this.IdConnectionGanador = idGanador;
                this.IdConnectionPerdedor = idPerdedor;
            }

            public Resultado(bool finalizo)
            {
                this.Finalizo = finalizo;
            }
        }

        public void RobarCarta(Jugador jugadorGanador,Jugador jugadorPerdedor)
        {
            if (jugadorPerdedor.Cartas.Count > 0)
            {
                
                var CartaARobar = jugadorPerdedor.Cartas.First();
                jugadorPerdedor.Cartas.Remove(CartaARobar);     
                jugadorGanador.Cartas.Add(CartaARobar);

            }
           
        }
       
        public void PasarCartaAtras(Jugador jugadorGanador, string idCarta)
        {
            //var cartaAtras = jugadorGanador.Cartas.Where(x => x.Codigo == idCarta).Single();
            var cartaAtras = jugadorGanador.Cartas[0];
            jugadorGanador.Cartas.Remove(cartaAtras);
            jugadorGanador.Cartas.Add(cartaAtras);
            
        }

        public void EliminarCartaColor(Jugador jugadorConCartaEspecial, Carta cartaDeColor)
        {
            jugadorConCartaEspecial.Cartas.Remove(cartaDeColor);
        }
        
    }

    
   
}