﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuegoCartas.Entidades
{
    public class Jugador
    {
        public string Nombre { get; set; }
        public string ConnectionId { get; set; }
        public List<Carta> Cartas { get; set; }


        public Jugador(string nombreJugador,string idConexion)
        {
            if (string.IsNullOrEmpty(nombreJugador))
                throw new ArgumentNullException("El nombre no puede estar vacío");
            if (string.IsNullOrEmpty(idConexion))
                throw new ArgumentNullException("El codigo no puede estar vacío");
            
            this.Cartas = new List<Carta>();
            this.Nombre = nombreJugador;
            this.ConnectionId = idConexion;

           
        }
        

    }
}
