﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace JuegoCartas.Entidades
{
    public class Juego //Es abstracto?
    {
        
        public List<Partida> Partidas { get; set; }
        public List<Mazo> Mazos { get; set; }
        public List<Jugador> Jugadores { get; set; }

        public Juego()
        {
            this.Partidas = new List<Partida>();
            this.Mazos = new List<Mazo>();
            this.Jugadores = new List<Jugador>();
            AgregarMazos();
        }
       
        
        public Jugador CrearJugador(string nombre, string idConexion)
        {
            var Jugador = new Jugador(nombre,idConexion);
            Jugador.Nombre = nombre;
            return Jugador;
        }

        public void CrearPartida(string nombreJugador, string nombrePartida, string nombreMazoSeleccionado, string idConexion)
        {
            var nuevaPartida = new Partida();

            if (Partidas.Exists(x => x.Nombre == nombrePartida))
            {
                throw new InvalidOperationException("La partida ya existe");
            }

            if (string.IsNullOrEmpty(nombreJugador))
            {
                throw new ArgumentNullException("La partida no puede contener un nombre nulo");
            }

            if (string.IsNullOrEmpty(nombreMazoSeleccionado))
            {
                throw new ArgumentNullException("Se debe seleccionar un mazo");
            }

            Jugador jugadorACrear = CrearJugador(nombreJugador, idConexion);
            nuevaPartida.Jugadores.Add(jugadorACrear);
            var mazo = this.Mazos.Where(x => x.Nombre == nombreMazoSeleccionado).Single();
            nuevaPartida.MazoSeleccionado= mazo;
            nuevaPartida.Nombre = nombrePartida;
            this.Partidas.Add(nuevaPartida);
        }

        public void UnirseAPartida(string nombrePartida, string nombreJugador, string idConexion)
        {
            var partidaExistente = ObtenerPartidaExistente(nombrePartida);
            if (partidaExistente.Jugadores.Count == 2)
            {
                throw new InvalidOperationException("no se puede unir a la partida, no es correcta la cantidad de jugadores");
            }
            Jugador jugador = new Jugador(nombreJugador, idConexion);

            partidaExistente.Jugadores.Add(jugador);
        }

        //Partidas

        public void AgregarMazos()
        {
            //Direccion de los mazos para los Test, con carpetaMazos= Hosting..... NO FUNCIONAN LOS TEST.
            //var carpetaMazos = Directory.GetDirectories(@"C:\Users\Victoria\Documents\tp-juego-cartas\JuegoCartas.Web\Mazos");

            var path = HostingEnvironment.MapPath("~/Mazos");
            var carpetaMazos = Directory.GetDirectories(path);
            foreach (var mazo in carpetaMazos)
            {
                var lineas = File.ReadAllLines(mazo + "\\Informacion.txt");
                var contador = 0;
                var nombreMazo = lineas[0];
                var atributosMazo = lineas[1];

                List<Atributo> listaAtributos = new List<Atributo>();
                Char delimitador = '|';
                string[] Atributos = atributosMazo.Split(delimitador);
                List<string> listaStringAtributos = new List<string>();
                var puntero = 0;
                foreach (var atributo in Atributos)
                {
                    if (puntero >= 2)
                    {
                        Atributo nuevoAtributo = new Atributo(atributo);
                        listaAtributos.Add(nuevoAtributo);
                        listaStringAtributos.Add(nuevoAtributo.Nombre);
                    }
                    puntero = puntero + 1;
                }
                contador = 0;

                Mazo nuevoMazo = new Mazo(nombreMazo, listaStringAtributos);

                
                foreach (var linea in lineas)
                {
                    contador = contador + 1;
                    if (contador >2)
                    {
                        string[] elementosCarta = linea.Split(delimitador);
                        Carta nuevaCarta = new Carta(elementosCarta[1], elementosCarta[0], Carta.TipoCarta.comun);
                        var indice = 0;
                        var numAtributo = 0;

                        foreach (var item in elementosCarta)
                        {
                            if (indice >1)
                            {
                                var valor = Convert.ToDecimal(item);
                                var nombreAtributo = listaAtributos[numAtributo].Nombre;
                                Atributo valorAtributo = new Atributo(nombreAtributo, valor);
                                nuevaCarta.Atributos.Add(valorAtributo);
                                numAtributo = numAtributo + 1;
                            }
                            indice = indice + 1;

                        }
                        nuevoMazo.Cartas.Add(nuevaCarta);

                    }

                }
 
                Carta cartaAmarilla = new Carta("Amarilla","amarilla", Carta.TipoCarta.amarilla);
                nuevoMazo.Cartas.Add(cartaAmarilla);
                Carta cartaRoja = new Carta("Roja","roja", Carta.TipoCarta.roja);
                nuevoMazo.Cartas.Add(cartaRoja);
                
                this.Mazos.Add(nuevoMazo);

            }
            
        }
            
        public Partida ObtenerPartidaDelJugador(string idconexion)
        {
            
            foreach (var partida in this.Partidas)
            {
                foreach (var jugador in partida.Jugadores)
                {
                    if (jugador.ConnectionId== idconexion)
                    {
                        return partida;
                    }
                }
            }
            
            throw new InvalidOperationException("No se puede obterner la partida");
        }
        
        public Partida ObtenerPartidaExistente(string partida)
        {
            Partida partidaExistente = Partidas.Where(x => x.Nombre == partida).Single();
            return partidaExistente;
        }

      
        public List<Partida> ObtenerListaDePartidasEnEspera()
        {
         
            List<Partida> partidasEspera = new List<Partida>();
            foreach (var partida in Partidas)
            {
                if (partida.Jugadores.Count == 1)
                {
                    partidasEspera.Add(partida);
            
                }
            }

            return partidasEspera;
        }

        public List<string> ObtenerMazos()
        {
            var mazos = new List<string>();
            foreach (Mazo mazo in Mazos)
            {
                mazos.Add(mazo.Nombre);
            }
            return mazos;
        }

        public Jugador ObtenerJugador(string idConexion)
        {
            return ObtenerPartidaDelJugador(idConexion).Jugadores.Where(x=>x.ConnectionId== idConexion).Single();
            
        }
        
    }
}
