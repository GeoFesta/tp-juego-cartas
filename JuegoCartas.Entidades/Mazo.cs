﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuegoCartas.Entidades
{
    public class Mazo
    {
        public string Nombre { get; set; }
        public List<Carta> Cartas { get; set; }
        public List<String> NombreAtributos { get; set; }

        
        public Mazo(string nombreMazo, List<String> listaNombreAtributos)
        {
            if (string.IsNullOrEmpty(nombreMazo))
                throw new ArgumentNullException("El nombre del mazo no puede estar vacío");

            this.Nombre = nombreMazo;
            this.Cartas = new List<Carta>();
            this.NombreAtributos = listaNombreAtributos;

        }
       
    }
}
