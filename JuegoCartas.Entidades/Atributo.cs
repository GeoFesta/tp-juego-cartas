﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuegoCartas.Entidades
{
    public class Atributo
    {
        public string Nombre { get; set; }
        public decimal Valor { get; set; }

        public Atributo(string nombreAtributo)
        {
            this.Nombre = nombreAtributo;
        }
        
        public Atributo(string nombreAtributo, decimal valorAtributo)
        {
            this.Nombre = nombreAtributo;
            this.Valor = valorAtributo;
        }
    }
}
