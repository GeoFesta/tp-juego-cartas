﻿using JuegoCartas.Entidades;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JuegoCartas.Web.Hubs
{
    public class JuegoHub : Hub
    {
        private static Juego juego = new Juego();

        public void CrearPartida(string usuario, string partida, string mazo)
        {           
            string idConnection = Context.ConnectionId;
            try
            {
                juego.CrearPartida(usuario, partida, mazo, idConnection);
                Partida nuevaPartida = juego.ObtenerPartidaExistente(partida);
                Clients.Others.agregarPartida(new
                {
                    Usuario = usuario,
                    Nombre = partida,
                    Mazo = mazo
                });
                Clients.Caller.esperarJugador();
            }
            catch (InvalidOperationException)
            {
                Clients.Client(idConnection).partidaDuplicada();
            }
            
        }

        public void UnirsePartida(string usuario, string partidaAUnirse)
        {
            ////logica para obtener partida, validar que no tenga otro jugador, asignar jugador dos , hacer lo que hay que hacer para tener lista la partida mezclar y repartir
            juego.UnirseAPartida(partidaAUnirse, usuario, Context.ConnectionId);
            Clients.All.eliminarPartida(partidaAUnirse);
            Partida partidaEnJuego = juego.ObtenerPartidaExistente(partidaAUnirse);
            partidaEnJuego.RepartirCartas();
            Jugador jugadorAUnirse = partidaEnJuego.Jugadores.Where(x => x.ConnectionId == Context.ConnectionId).Single();
            Jugador jugadorCreador = partidaEnJuego.Jugadores.Where(x=> x.ConnectionId!= Context.ConnectionId).Single();
            Mazo mazo = partidaEnJuego.MazoSeleccionado;
            Clients.Client(jugadorCreador.ConnectionId).dibujarTablero(jugadorCreador, jugadorAUnirse,mazo);
            Clients.Client(jugadorAUnirse.ConnectionId).dibujarTablero(jugadorCreador, jugadorAUnirse,mazo);
        }

        public void ObtenerPartidas()
        {

            Clients.Caller.agregarPartidas(juego.ObtenerListaDePartidasEnEspera().Select(x => new
            {
                Nombre = x.Nombre,
                Mazo = x.MazoSeleccionado.Nombre,
                Usuario = x.Jugadores[0].Nombre
            }));
        }

        public void ObtenerMazos()
        {
            List<string> mazos = new List<string>();
            foreach (var mazo in juego.Mazos)
            {
                mazos.Add(mazo.Nombre);
            }
            Clients.Caller.agregarMazos(mazos);
        }

        public void Cantar(string idAtributo, string idCarta)
        {
            Partida partida = juego.ObtenerPartidaDelJugador(Context.ConnectionId);
            Partida.Resultado resultado =  partida.CompararCartas(idAtributo, Context.ConnectionId, idCarta);
            switch (resultado.TipoResultado)
            {
                case Partida.tipoResultado.normal:
                    Clients.Client(resultado.IdConnectionPerdedor).perderMano();
                    Clients.Client(resultado.IdConnectionGanador).ganarMano();
                    break;
                case Partida.tipoResultado.amarilla:
                    Clients.Client(resultado.IdConnectionGanador).ganarManoPorTarjetaAmarilla();
                    Clients.Client(resultado.IdConnectionPerdedor).perderManoPorTarjetaAmarilla();
                    break;
                case Partida.tipoResultado.roja:
                    Clients.Client(resultado.IdConnectionGanador).ganarManoPorTarjetaRoja();
                    Clients.Client(resultado.IdConnectionPerdedor).perderManoPorTarjetaRoja();
                    break;
            }
            if (resultado.Finalizo)
            {
                Clients.Client(resultado.IdConnectionPerdedor).perder();
                Clients.Client(resultado.IdConnectionGanador).ganar();
            }
        }

    }
}