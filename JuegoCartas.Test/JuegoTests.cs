﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JuegoCartas.Entidades;
using System.Collections.Generic;

namespace JuegoCartas.Test
{
    [TestClass]
    public class JuegoTests
    {
        [TestMethod]
        public void DeberiaPoderCrearElJuego()
        {
            Juego juego = new Juego();
            Assert.IsTrue(juego.Partidas != null && juego.Mazos != null && juego.Jugadores != null);
        }

        [TestMethod]
        public void DeberiaPoderCrearUnJugador()
        {
            var jugador = new Jugador("Geo", "conexion000");

            Assert.AreEqual("Geo", jugador.Nombre);
            Assert.AreEqual("conexion000", jugador.ConnectionId);
            Assert.IsNotNull(jugador.Cartas);

        }

        [TestMethod]
        public void DeberiaPoderCrearPartida()
        {
            var nombre = "partida";
            var jugador = "Yanet";
            var mazo = "Personajes Marvel";
            var id = "conexion000";
            Juego juego = new Juego();
            juego.CrearPartida(jugador, nombre, mazo, id);
            Assert.IsTrue((juego.Partidas.Count) != 0);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void NoDeberiaCrearPartidaConNombreRepetido()
        {
              var juego = new Juego();
              juego.CrearPartida("nombre", "partida1", "Personajes Marvel", "1111");
              juego.CrearPartida("nombre", "partida1", "Personajes Marvel", "41111");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NoDeberiaCrearPartidaConNombreNulo()
        {
            var juego = new Juego();
            juego.CrearPartida("", "partida8", "Personajes Marvel", "1255");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NoDeberiaCrearPartidaConMazoNulo()
        {
            var juego = new Juego();
            juego.CrearPartida("user1", "partida8", "", "1255");

        }

        [TestMethod]
        public void DeberiaObtenerPartidaPasandoIdJugador()
        {
            Juego juego = new Juego();
            juego.CrearPartida("user", "partida1", "Personajes Marvel", "1");
            Assert.IsNotNull(juego.ObtenerPartidaDelJugador("1"));
        }

        [TestMethod]
        public void JugadorDeberiaPoderUnirseAPartida()
        {
            var juego = new Juego();
            juego.CrearPartida("user1", "partida8", "Personajes Marvel", "1");
            juego.UnirseAPartida("partida8", "user2", "2");
            var partida = juego.ObtenerPartidaDelJugador("1");
            Assert.IsTrue(partida.Jugadores.Count == 2);
            
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void NoDeberiaUnirseSiNoHayDos()
        {
            var juego = new Juego();
            juego.CrearPartida("geo", "partida1", "Personajes Marvel Prueba", "1");
            juego.UnirseAPartida("partida1", "Personajes Marvel Prueba", "2");
            juego.UnirseAPartida("partida1", "Personajes Marvel Prueba", "3");

        }

        [TestMethod] 
        public void DeberiaObtenerPartidaPasandoNombrePartida()
        {
            Juego juego = new Juego();
            juego.CrearPartida("viki", "partida1", "Personajes Marvel", "111");
            Assert.IsNotNull(juego.ObtenerPartidaExistente("partida1"));
        }

        [TestMethod]
        public void DeberiaObtenerListaPartidaEnEspera()
        {
            var juego = new Juego();
            juego.CrearPartida("nombre", "partida1", "Personajes Marvel", "1111");
            juego.CrearPartida("nombre", "partida2", "Personajes Marvel", "41111");
            Assert.IsTrue(juego.ObtenerListaDePartidasEnEspera().Count == 2);
        }

        [TestMethod]
        public void DeberiaObtenerJugadorPorId()
        {
            var juego = new Juego();
            juego.CrearPartida("viki", "partida1", "Personajes Marvel", "1111");
            juego.CrearPartida("nombre", "partida2", "Personajes Marvel", "41111");
            Assert.IsTrue(juego.ObtenerJugador("1111").Nombre == "viki");
        }

        [TestMethod]
        public void DeberiaPoderAgregarMazos()
        {
            var juego = new Juego();
            Assert.IsFalse(juego.Mazos.Count == 0);
        }

        [TestMethod]
        public void DeberiaObtenerListaMazos()
        {
            var juego = new Juego();
            Assert.IsNotNull(juego.ObtenerMazos());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void NoDeberiaPoderObtenerPartidaDeJugador()
        {
            Juego juego = new Juego();
            juego.CrearPartida("geo", "partida", "Personajes Marvel", "111");
            juego.ObtenerPartidaDelJugador("8");
        }
    }
    
}
