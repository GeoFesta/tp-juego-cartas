﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JuegoCartas.Entidades;

namespace JuegoCartas.Test
{

    [TestClass]
    public class CartaTest
    {
        [TestMethod]
        public void DeberiaCrearUnaCarta()
        {
            Carta carta = new Carta("carta1", "1", Carta.TipoCarta.comun);
            Assert.IsTrue(carta.Nombre == "carta1" && carta.Tipo == Carta.TipoCarta.comun && carta.Codigo == "1");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NoDeberiaCrearCartaConNombreNulo()
        {
            Carta carta = new Carta("", "1", Carta.TipoCarta.comun);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NoDeberiaCrearCartaConCodigoNulo()
        {
            Carta carta = new Carta("carta1","", Carta.TipoCarta.comun);
        }
    }
}
