﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JuegoCartas.Entidades;

namespace JuegoCartas.Test
{
    
    [TestClass]
    public class JugadorTest
    {
       
        [TestMethod]
        public void DeberiaPoderCrearJugador()
        {
            Jugador jugador = new Jugador("viki", "1");
            Assert.IsTrue(jugador.Nombre == "viki" && jugador.ConnectionId == "1" && jugador.Cartas != null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NoDeberiaPoderCrearJugadorSinNombre()
        {
            Jugador jugador = new Jugador("","1");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NoDeberiaPoderCrearJugadorSinConnectionId()
        {
            Jugador jugador = new Jugador("Viki", "");
        }
    }
}
