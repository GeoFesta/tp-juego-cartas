﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JuegoCartas.Entidades;
using System.Linq;

namespace JuegoCartas.Test
{
    [TestClass]
    public class PartidaTest
    {
        [TestMethod]
        public void PermiteMezclarMazo()
        {

            Juego juego = new Juego();
            juego.AgregarMazos();
            Mazo mazo = juego.Mazos[0];

            var mazoOriginal = mazo.Cartas;
            Partida partidanueva = new Partida();
            partidanueva.Nombre = "partida nueva";
            partidanueva.MazoSeleccionado = mazo;
            Jugador jugador1 = new Jugador("viki", "conexion000");
            Jugador jugador2 = new Jugador("yanet", "conexion001");
            partidanueva.Jugadores.Add(jugador1);
            partidanueva.Jugadores.Add(jugador2);
            Assert.AreNotEqual(mazoOriginal, partidanueva.MezclarCartas());
        }        

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NoDeberiaMezclarSinUnJugador()
        {
            Juego juego = new Juego();
            juego.AgregarMazos();
            Mazo mazo = juego.Mazos[0];

            var Mazooriginal = mazo.Cartas;
            Partida partidanueva = new Partida();
            partidanueva.Nombre = "partida nueva";
            partidanueva.MazoSeleccionado = mazo;
            Jugador jugador1 = new Jugador("viki","conexion022");
            Jugador jugador2 = new Jugador("yanet","conexion026");
            partidanueva.Jugadores.Add(jugador1);
            partidanueva.MezclarCartas();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NoDeberiaMezclarSinMazo()
        {
            Juego juego = new Juego();
            Partida partida = new Partida();
            Jugador jugador1 = new Jugador("viki", "conexion022");
            Jugador jugador2 = new Jugador("yanet", "conexion026");
            partida.Jugadores.Add(jugador1);
            partida.Jugadores.Add(jugador2);
            partida.MezclarCartas();
           
        }

        [TestMethod]
        public void PermiteRepartir()
        {
            Juego juego = new Juego();
            juego.AgregarMazos();
            Mazo mazo = juego.Mazos[0];

            Partida partidanueva = new Partida();
            partidanueva.Nombre = "partida nueva";
            partidanueva.MazoSeleccionado = mazo;
            Jugador jugador1 = new Jugador("viki","conexion123");
            Jugador jugador2 = new Jugador("yanet","conexion456");
            partidanueva.Jugadores.Add(jugador1);
            partidanueva.Jugadores.Add(jugador2);
            partidanueva.MezclarCartas();
            partidanueva.RepartirCartas();
            Assert.IsNotNull(partidanueva.Jugadores[0].Cartas);
            Assert.IsNotNull(partidanueva.Jugadores[1].Cartas);
        }
        
        
        [TestMethod]
        public void JugadoresDeberianTenerMismaCantidadCartas()
        {
            Juego juego = new Juego();
            Jugador j1 = new Jugador("Yanet","conexion123");
            Jugador j2 = new Jugador("Geo","conexion456");
            Partida partida = new Partida();
            partida.MazoSeleccionado = juego.Mazos.Where(x => x.Nombre == "Personajes Marvel Prueba").Single();
            partida.Nombre = "Juego Marvel";
            partida.Jugadores.Add(j1);
            partida.Jugadores.Add(j2);
            partida.MezclarCartas();
            Partida.ListasCartas listasCartas= partida.RepartirCartas();
            Assert.AreEqual(listasCartas.CartasJugador1.Count, listasCartas.CartasJugador2.Count);
            
        }

        [TestMethod]
        public void JugadoresNoDeberiaTenerMismasCartas()
        {
            Juego juego = new Juego();
            juego.AgregarMazos();
            Jugador j1 = new Jugador("Yanet","conexion555");
            Jugador j2 = new Jugador("Geo","conexion666");
            Partida partida = new Partida();
            partida.MazoSeleccionado = juego.Mazos[0];
            partida.Nombre = partida.MazoSeleccionado.Nombre;
            partida.Jugadores.Add(j1);
            partida.Jugadores.Add(j2);
            partida.MezclarCartas();
            Partida.ListasCartas listasCartas = partida.RepartirCartas();
            var existe = 0;
            foreach (var carta in listasCartas.CartasJugador1)
            {
                foreach (var carta2 in listasCartas.CartasJugador2)
                {
                    if (carta.Codigo == carta2.Codigo)
                    {
                        existe = 1;
                    }
                }
            }

            Assert.AreEqual(0, existe);
        }

        [TestMethod]
        public void JugadorQueCantaGanaConComun()
        {
            Juego juego = new Juego();
            juego.CrearPartida("Viki", "partida", "Personajes Marvel Prueba", "1");
            juego.UnirseAPartida("partida", "Geo", "2");
            Partida partida = juego.ObtenerPartidaDelJugador("1");
            Jugador jugadorQueCanta = partida.Jugadores.Where(x => x.ConnectionId == "1").Single();
            Jugador jugadorQueEspera = partida.Jugadores.Where(x => x.ConnectionId == "2").Single();

            var carta1 = new Carta("Thor", "cn1", Carta.TipoCarta.comun);
            carta1.Atributos.Add(new Atributo("Fuerza", 1000));

            var carta2 = new Carta("Spiderman", "cn2", Carta.TipoCarta.comun);
            carta2.Atributos.Add(new Atributo("Fuerza", 100));

            var carta3 = new Carta("Daredevil", "cn3", Carta.TipoCarta.comun);
            carta3.Atributos.Add(new Atributo("Fuerza", 995));

            var carta4 = new Carta("Hulk", "cn4", Carta.TipoCarta.comun);
            carta4.Atributos.Add(new Atributo("Fuerza", 4000));

            jugadorQueCanta.Cartas.Add(carta1);
            jugadorQueCanta.Cartas.Add(carta2);
            jugadorQueEspera.Cartas.Add(carta3);
            jugadorQueEspera.Cartas.Add(carta4);

            Partida.Resultado resultado=partida.CompararCartas("Fuerza", "1", "cn1");
            Assert.IsTrue(resultado.IdConnectionGanador == "1" && resultado.IdConnectionPerdedor=="2" && resultado.TipoResultado == Partida.tipoResultado.normal);

        }

        [TestMethod]
        public void JugadorQueEsperaGanaConComun()
        {
            Juego juego = new Juego();
            juego.CrearPartida("Viki", "partida", "Personajes Marvel Prueba", "1");
            juego.UnirseAPartida("partida", "Geo", "2");
            Partida partida = juego.ObtenerPartidaDelJugador("1");
            Jugador jugadorQueCanta = partida.Jugadores.Where(x => x.ConnectionId == "1").Single();
            Jugador jugadorQueEspera = partida.Jugadores.Where(x => x.ConnectionId == "2").Single();

            var carta1 = new Carta("Thor", "cn1", Carta.TipoCarta.comun);
            carta1.Atributos.Add(new Atributo("Fuerza", 1000));

            var carta2 = new Carta("Spiderman", "cn2", Carta.TipoCarta.comun);
            carta2.Atributos.Add(new Atributo("Fuerza", 100));

            var carta3 = new Carta("Daredevil", "cn3", Carta.TipoCarta.comun);
            carta3.Atributos.Add(new Atributo("Fuerza", 995));

            var carta4 = new Carta("Hulk", "cn4", Carta.TipoCarta.comun);
            carta4.Atributos.Add(new Atributo("Fuerza", 4000));

            jugadorQueCanta.Cartas.Add(carta3);
            jugadorQueCanta.Cartas.Add(carta4);
            jugadorQueEspera.Cartas.Add(carta1);
            jugadorQueEspera.Cartas.Add(carta2);

            Partida.Resultado resultado = partida.CompararCartas("Fuerza", "1", "cn1");
            Assert.IsTrue(resultado.IdConnectionGanador == "2" && resultado.IdConnectionPerdedor == "1" && resultado.TipoResultado == Partida.tipoResultado.normal);
        }

        [TestMethod]
        public void JugadorQueCantaGanaConAmarilla()
        {
            Juego juego = new Juego();
            juego.CrearPartida("Viki", "partida", "Personajes Marvel Prueba", "1");
            juego.UnirseAPartida("partida", "Geo", "2");
            Partida partida = juego.ObtenerPartidaDelJugador("1");
            Jugador jugadorQueCanta = partida.Jugadores.Where(x => x.ConnectionId == "1").Single();
            Jugador jugadorQueEspera = partida.Jugadores.Where(x => x.ConnectionId == "2").Single();

            var carta1 = new Carta("Thor", "cn1", Carta.TipoCarta.comun);
            carta1.Atributos.Add(new Atributo("Fuerza", 1000));

            var carta2 = new Carta("Spiderman", "cn2", Carta.TipoCarta.comun);
            carta2.Atributos.Add(new Atributo("Fuerza", 100));

            var carta3 = new Carta("Daredevil", "cn3", Carta.TipoCarta.comun);
            carta3.Atributos.Add(new Atributo("Fuerza", 995));

            var carta4 = new Carta("Hulk", "cn4", Carta.TipoCarta.comun);
            carta4.Atributos.Add(new Atributo("Fuerza", 4000));

            var carta5 = new Carta("Amarilla", "cn5", Carta.TipoCarta.amarilla);


            jugadorQueCanta.Cartas.Add(carta5);
            jugadorQueCanta.Cartas.Add(carta1);
            jugadorQueCanta.Cartas.Add(carta2);
            jugadorQueEspera.Cartas.Add(carta3);
            jugadorQueEspera.Cartas.Add(carta4);

            Partida.Resultado resultado = partida.CompararCartas("Fuerza", "1", "cn5");
            Assert.IsTrue(resultado.IdConnectionGanador == "1" && resultado.IdConnectionPerdedor == "2" && resultado.TipoResultado == Partida.tipoResultado.amarilla);
        }
        [TestMethod]
        public void JugadorQueEsperaGanaConAmarilla()
        {
            Juego juego = new Juego();
            juego.CrearPartida("Viki", "partida", "Personajes Marvel Prueba", "1");
            juego.UnirseAPartida("partida", "Geo", "2");
            Partida partida = juego.ObtenerPartidaDelJugador("1");
            //partida.RepartirCartas();
            Jugador jugadorQueCanta = partida.Jugadores.Where(x => x.ConnectionId == "1").Single();
            Jugador jugadorQueEspera = partida.Jugadores.Where(x => x.ConnectionId == "2").Single();

            var carta1 = new Carta("Thor", "cn1", Carta.TipoCarta.comun);
            carta1.Atributos.Add(new Atributo("Fuerza", 1000));

            var carta2 = new Carta("Spiderman", "cn2", Carta.TipoCarta.comun);
            carta2.Atributos.Add(new Atributo("Fuerza", 100));

            var carta3 = new Carta("Daredevil", "cn3", Carta.TipoCarta.comun);
            carta3.Atributos.Add(new Atributo("Fuerza", 995));

            var carta4 = new Carta("Hulk", "cn4", Carta.TipoCarta.comun);
            carta4.Atributos.Add(new Atributo("Fuerza", 4000));

            var carta5 = new Carta("Amarilla", "cn5", Carta.TipoCarta.amarilla);


            jugadorQueCanta.Cartas.Add(carta1);
            jugadorQueCanta.Cartas.Add(carta2);
            jugadorQueEspera.Cartas.Add(carta5);
            jugadorQueEspera.Cartas.Add(carta3);
            jugadorQueEspera.Cartas.Add(carta4);
            
            Partida.Resultado resultado = partida.CompararCartas("Fuerza", "1", "cn1");
            Assert.IsTrue(resultado.IdConnectionGanador == "2" && resultado.IdConnectionPerdedor == "1" && resultado.TipoResultado == Partida.tipoResultado.amarilla);
        }
        [TestMethod]
        public void JugadorQueCantaGanaConRoja()
        {
            Juego juego = new Juego();
            juego.CrearPartida("Viki", "partida", "Personajes Marvel Prueba", "1");
            juego.UnirseAPartida("partida", "Geo", "2");
            Partida partida = juego.ObtenerPartidaDelJugador("1");
            Jugador jugadorQueCanta = partida.Jugadores.Where(x => x.ConnectionId == "1").Single();
            Jugador jugadorQueEspera = partida.Jugadores.Where(x => x.ConnectionId == "2").Single();

            var carta1 = new Carta("Thor", "cn1", Carta.TipoCarta.comun);
            carta1.Atributos.Add(new Atributo("Fuerza", 1000));

            var carta2 = new Carta("Spiderman", "cn2", Carta.TipoCarta.comun);
            carta2.Atributos.Add(new Atributo("Fuerza", 100));

            var carta3 = new Carta("Daredevil", "cn3", Carta.TipoCarta.comun);
            carta3.Atributos.Add(new Atributo("Fuerza", 995));

            var carta4 = new Carta("Hulk", "cn4", Carta.TipoCarta.comun);
            carta4.Atributos.Add(new Atributo("Fuerza", 4000));

            var carta5 = new Carta("Amarilla", "cn5", Carta.TipoCarta.amarilla);

            var carta6 = new Carta("Roja", "cn6", Carta.TipoCarta.roja);

            jugadorQueCanta.Cartas.Add(carta6);
            jugadorQueCanta.Cartas.Add(carta1);
            jugadorQueCanta.Cartas.Add(carta2);
            jugadorQueCanta.Cartas.Add(carta5);
            jugadorQueEspera.Cartas.Add(carta3);
            jugadorQueEspera.Cartas.Add(carta4);

            Partida.Resultado resultado = partida.CompararCartas("", "1", "cn6");
            Assert.IsTrue(resultado.IdConnectionGanador == "1" && resultado.IdConnectionPerdedor == "2" && resultado.TipoResultado == Partida.tipoResultado.roja);

        }
        [TestMethod]
        public void JugadorQueEsperaGanaConRoja()
        {
            Juego juego = new Juego();
            juego.CrearPartida("Viki", "partida", "Personajes Marvel Prueba", "1");
            juego.UnirseAPartida("partida", "Geo", "2");
            Partida partida = juego.ObtenerPartidaDelJugador("1");
            Jugador jugadorQueCanta = partida.Jugadores.Where(x => x.ConnectionId == "1").Single();
            Jugador jugadorQueEspera = partida.Jugadores.Where(x => x.ConnectionId == "2").Single();

            var carta1 = new Carta("Thor", "cn1", Carta.TipoCarta.comun);
            carta1.Atributos.Add(new Atributo("Fuerza", 1000));

            var carta2 = new Carta("Spiderman", "cn2", Carta.TipoCarta.comun);
            carta2.Atributos.Add(new Atributo("Fuerza", 100));

            var carta3 = new Carta("Daredevil", "cn3", Carta.TipoCarta.comun);
            carta3.Atributos.Add(new Atributo("Fuerza", 995));

            var carta4 = new Carta("Hulk", "cn4", Carta.TipoCarta.comun);
            carta4.Atributos.Add(new Atributo("Fuerza", 4000));

            var carta5 = new Carta("Amarilla", "cn5", Carta.TipoCarta.amarilla);

            var carta6 = new Carta("Roja", "cn6", Carta.TipoCarta.roja);

            jugadorQueCanta.Cartas.Add(carta1);
            jugadorQueCanta.Cartas.Add(carta2);
            jugadorQueCanta.Cartas.Add(carta5);
            jugadorQueEspera.Cartas.Add(carta6);
            jugadorQueEspera.Cartas.Add(carta3);
            jugadorQueEspera.Cartas.Add(carta4);

            Partida.Resultado resultado = partida.CompararCartas("Fuerza", "1", "cn1");
            Assert.IsTrue(resultado.IdConnectionGanador == "2" && resultado.IdConnectionPerdedor == "1" && resultado.TipoResultado == Partida.tipoResultado.roja);

        }
        [TestMethod]
        public void JugadorQueCantaGanaConRojaAAmarilla()
        {
            Juego juego = new Juego();
            juego.CrearPartida("Viki", "partida", "Personajes Marvel Prueba", "1");
            juego.UnirseAPartida("partida", "Geo", "2");
            Partida partida = juego.ObtenerPartidaDelJugador("1");
            Jugador jugadorQueCanta = partida.Jugadores.Where(x => x.ConnectionId == "1").Single();
            Jugador jugadorQueEspera = partida.Jugadores.Where(x => x.ConnectionId == "2").Single();

            var carta1 = new Carta("Thor", "cn1", Carta.TipoCarta.comun);
            carta1.Atributos.Add(new Atributo("Fuerza", 1000));

            var carta2 = new Carta("Spiderman", "cn2", Carta.TipoCarta.comun);
            carta2.Atributos.Add(new Atributo("Fuerza", 100));

            var carta3 = new Carta("Daredevil", "cn3", Carta.TipoCarta.comun);
            carta3.Atributos.Add(new Atributo("Fuerza", 995));

            var carta4 = new Carta("Hulk", "cn4", Carta.TipoCarta.comun);
            carta4.Atributos.Add(new Atributo("Fuerza", 4000));

            var carta5 = new Carta("Amarilla", "cn5", Carta.TipoCarta.amarilla);

            var carta6 = new Carta("Roja", "cn6", Carta.TipoCarta.roja);

            jugadorQueCanta.Cartas.Add(carta6);
            jugadorQueCanta.Cartas.Add(carta1);
            jugadorQueCanta.Cartas.Add(carta2);
            jugadorQueEspera.Cartas.Add(carta5);
            jugadorQueEspera.Cartas.Add(carta3);
            jugadorQueEspera.Cartas.Add(carta4);

            Partida.Resultado resultado = partida.CompararCartas("", "1", "cn6");
            Assert.IsTrue(resultado.IdConnectionGanador == "1" && resultado.IdConnectionPerdedor == "2" && resultado.TipoResultado == Partida.tipoResultado.roja);
        }

        [TestMethod]
        public void JugadorQueEsperaGanaConRojaAAmarilla()
        {
            Juego juego = new Juego();
            juego.CrearPartida("Viki", "partida", "Personajes Marvel Prueba", "1");
            juego.UnirseAPartida("partida", "Geo", "2");
            Partida partida = juego.ObtenerPartidaDelJugador("1");
            Jugador jugadorQueCanta = partida.Jugadores.Where(x => x.ConnectionId == "1").Single();
            Jugador jugadorQueEspera = partida.Jugadores.Where(x => x.ConnectionId == "2").Single();

            var carta1 = new Carta("Thor", "cn1", Carta.TipoCarta.comun);
            carta1.Atributos.Add(new Atributo("Fuerza", 1000));

            var carta2 = new Carta("Spiderman", "cn2", Carta.TipoCarta.comun);
            carta2.Atributos.Add(new Atributo("Fuerza", 100));

            var carta3 = new Carta("Daredevil", "cn3", Carta.TipoCarta.comun);
            carta3.Atributos.Add(new Atributo("Fuerza", 995));

            var carta4 = new Carta("Hulk", "cn4", Carta.TipoCarta.comun);
            carta4.Atributos.Add(new Atributo("Fuerza", 4000));

            var carta5 = new Carta("Amarilla", "cn5", Carta.TipoCarta.amarilla);

            var carta6 = new Carta("Roja", "cn6", Carta.TipoCarta.roja);

            jugadorQueCanta.Cartas.Add(carta5);
            jugadorQueCanta.Cartas.Add(carta1);
            jugadorQueCanta.Cartas.Add(carta2);
            jugadorQueEspera.Cartas.Add(carta6);
            jugadorQueEspera.Cartas.Add(carta3);
            jugadorQueEspera.Cartas.Add(carta4);

            Partida.Resultado resultado = partida.CompararCartas("", "1", "cn6");
            Assert.IsTrue(resultado.IdConnectionGanador == "2" && resultado.IdConnectionPerdedor == "1" && resultado.TipoResultado==Partida.tipoResultado.roja);
        }

        [TestMethod]
        public void JugadorQueCantaPierde()
        {
            Juego juego = new Juego();
            juego.CrearPartida("Viki", "partida", "Personajes Marvel Prueba", "1");
            juego.UnirseAPartida("partida", "Geo", "2");
            Partida partida = juego.ObtenerPartidaDelJugador("1");
            Jugador jugadorQueCanta = partida.Jugadores.Where(x => x.ConnectionId == "1").Single();
            Jugador jugadorQueEspera = partida.Jugadores.Where(x => x.ConnectionId == "2").Single();

            var carta1 = new Carta("Thor", "cn1", Carta.TipoCarta.comun);
            carta1.Atributos.Add(new Atributo("Fuerza", 1000));

            var carta2 = new Carta("Spiderman", "cn2", Carta.TipoCarta.comun);
            carta2.Atributos.Add(new Atributo("Fuerza", 100));

            var carta3 = new Carta("Daredevil", "cn3", Carta.TipoCarta.comun);
            carta3.Atributos.Add(new Atributo("Fuerza", 995));

            var carta4 = new Carta("Hulk", "cn4", Carta.TipoCarta.comun);
            carta4.Atributos.Add(new Atributo("Fuerza", 4000));

            var carta5 = new Carta("Amarilla", "cn5", Carta.TipoCarta.amarilla);

            var carta6 = new Carta("Roja", "cn6", Carta.TipoCarta.roja);


            jugadorQueCanta.Cartas.Add(carta1);

            jugadorQueEspera.Cartas.Add(carta3);
           
           
            Partida.Resultado resultado = partida.CompararCartas("Fuerza", "1", "cn1");
            
            Assert.IsTrue(resultado.IdConnectionGanador == "1" && resultado.IdConnectionPerdedor == "2" && resultado.Finalizo==true);
        }

        [TestMethod]
        public void JugadorQueEsperaPierde()
        {
            Juego juego = new Juego();
            juego.CrearPartida("Viki", "partida", "Personajes Marvel Prueba", "1");
            juego.UnirseAPartida("partida", "Geo", "2");
            Partida partida = juego.ObtenerPartidaDelJugador("1");
            Jugador jugadorQueCanta = partida.Jugadores.Where(x => x.ConnectionId == "1").Single();
            Jugador jugadorQueEspera = partida.Jugadores.Where(x => x.ConnectionId == "2").Single();

            var carta1 = new Carta("Thor", "cn1", Carta.TipoCarta.comun);
            carta1.Atributos.Add(new Atributo("Fuerza", 1000));

            var carta2 = new Carta("Spiderman", "cn2", Carta.TipoCarta.comun);
            carta2.Atributos.Add(new Atributo("Fuerza", 100));

            var carta3 = new Carta("Daredevil", "cn3", Carta.TipoCarta.comun);
            carta3.Atributos.Add(new Atributo("Fuerza", 995));

            var carta4 = new Carta("Hulk", "cn4", Carta.TipoCarta.comun);
            carta4.Atributos.Add(new Atributo("Fuerza", 4000));

            var carta5 = new Carta("Amarilla", "cn5", Carta.TipoCarta.amarilla);

            var carta6 = new Carta("Roja", "cn6", Carta.TipoCarta.roja);

           
            jugadorQueCanta.Cartas.Add(carta1);
            jugadorQueEspera.Cartas.Add(carta4);
           
            
            Partida.Resultado resultado = partida.CompararCartas("Fuerza", "1", "cn4");
            Assert.IsTrue(resultado.IdConnectionGanador == "2" && resultado.IdConnectionPerdedor == "1" && resultado.Finalizo==true);
        }

        [TestMethod]
        public void RobaBienCartas()
        {
            Partida partida = new Partida();
            partida.Jugadores.Add(new Jugador("viki", "1"));
            partida.Jugadores.Add(new Jugador("yanet", "2"));
            Jugador jugador1 = partida.Jugadores[0];
            Jugador jugador2 = partida.Jugadores[1];

            var carta1 = new Carta("Thor", "cn1", Carta.TipoCarta.comun);
            carta1.Atributos.Add(new Atributo("Fuerza", 1000));

            var carta2 = new Carta("Spiderman", "cn2", Carta.TipoCarta.comun);
            carta2.Atributos.Add(new Atributo("Fuerza", 100));

            var carta3 = new Carta("Daredevil", "cn3", Carta.TipoCarta.comun);
            carta3.Atributos.Add(new Atributo("Fuerza", 995));

            var carta4 = new Carta("Hulk", "cn4", Carta.TipoCarta.comun);
            carta4.Atributos.Add(new Atributo("Fuerza", 4000));

            var carta5 = new Carta("Amarilla", "cn5", Carta.TipoCarta.amarilla);

            var carta6 = new Carta("Roja", "cn6", Carta.TipoCarta.roja);

            jugador1.Cartas.Add(carta1);
            jugador1.Cartas.Add(carta2);
            jugador2.Cartas.Add(carta3);
            jugador2.Cartas.Add(carta4);

            partida.RobarCarta(jugador1, jugador2);
            Assert.IsTrue(jugador1.Cartas[0] == carta1 && jugador1.Cartas[1] == carta2 && jugador1.Cartas[2] == carta3 && jugador2.Cartas[0] == carta4);

        }
        
        [TestMethod]
        public void DeberiaEliminarCartaDeColor()
        {
            Partida partida = new Partida();
            partida.Jugadores.Add(new Jugador("viki", "1"));
            partida.Jugadores.Add(new Jugador("yanet", "2"));
            Jugador jugador1 = partida.Jugadores[0];
            Jugador jugador2 = partida.Jugadores[1];

            var carta1 = new Carta("Thor", "cn1", Carta.TipoCarta.comun);
            carta1.Atributos.Add(new Atributo("Fuerza", 1000));

            var carta2 = new Carta("Spiderman", "cn2", Carta.TipoCarta.comun);
            carta2.Atributos.Add(new Atributo("Fuerza", 100));

            var carta3 = new Carta("Daredevil", "cn3", Carta.TipoCarta.comun);
            carta3.Atributos.Add(new Atributo("Fuerza", 995));

            var carta4 = new Carta("Hulk", "cn4", Carta.TipoCarta.comun);
            carta4.Atributos.Add(new Atributo("Fuerza", 4000));

            var carta5 = new Carta("Amarilla", "cn5", Carta.TipoCarta.amarilla);

            var carta6 = new Carta("Roja", "cn6", Carta.TipoCarta.roja);

            jugador1.Cartas.Add(carta5);
            jugador1.Cartas.Add(carta1);
            jugador2.Cartas.Add(carta3);
            jugador2.Cartas.Add(carta6);

            partida.EliminarCartaColor(jugador1, carta5);
            Assert.IsTrue(jugador1.Cartas[0] == carta1 && jugador1.Cartas.Count==1);
            
        }

        [TestMethod]
        public void DeberiaSetearFinalizoEnTrue()
        {
            Partida.Resultado resultado = new Partida.Resultado(true);
            Assert.IsTrue(resultado.Finalizo == true);
        }

        [TestMethod]
        public void DeberiaPasarCartaJugadorAtras()
        {
            Partida partida = new Partida();
            partida.Jugadores.Add(new Jugador("viki", "1"));
            partida.Jugadores.Add(new Jugador("yanet", "2"));
            Jugador jugador1 = partida.Jugadores[0];
            Jugador jugador2 = partida.Jugadores[1];

            var carta1 = new Carta("Thor", "cn1", Carta.TipoCarta.comun);
            carta1.Atributos.Add(new Atributo("Fuerza", 1000));

            var carta2 = new Carta("Spiderman", "cn2", Carta.TipoCarta.comun);
            carta2.Atributos.Add(new Atributo("Fuerza", 100));

            var carta3 = new Carta("Daredevil", "cn3", Carta.TipoCarta.comun);
            carta3.Atributos.Add(new Atributo("Fuerza", 995));

            var carta4 = new Carta("Hulk", "cn4", Carta.TipoCarta.comun);
            carta4.Atributos.Add(new Atributo("Fuerza", 4000));

            var carta5 = new Carta("Amarilla", "cn5", Carta.TipoCarta.amarilla);

            var carta6 = new Carta("Roja", "cn6", Carta.TipoCarta.roja);

            jugador1.Cartas.Add(carta1);
            jugador1.Cartas.Add(carta5);
            jugador2.Cartas.Add(carta3);
            jugador2.Cartas.Add(carta6);

            partida.PasarCartaAtras(jugador1, "cn1");
            Assert.IsTrue(jugador1.Cartas.Last()== carta1);

        }

    }
    
}
