﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JuegoCartas.Entidades;

namespace JuegoCartas.Test
{
    [TestClass]
    public class MazoTest
    {
       
        #region Atributos de prueba adicionales
        //
        // Puede usar los siguientes atributos adicionales conforme escribe las pruebas:
        //
        // Use ClassInitialize para ejecutar el código antes de ejecutar la primera prueba en la clase
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup para ejecutar el código una vez ejecutadas todas las pruebas en una clase
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Usar TestInitialize para ejecutar el código antes de ejecutar cada prueba 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup para ejecutar el código una vez ejecutadas todas las pruebas
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void DeberiaCrearMazo()
        {
            List<string> list = new List<string>();
            Mazo mazo = new Mazo("Mazo", list);
            Assert.IsTrue(mazo.Nombre == "Mazo"&& mazo.Cartas!= null && mazo.NombreAtributos==list);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NoDeberiaCrearMazoConNombreNulo()
        {
            Mazo mazo = new Mazo("", new List<string>());
        }
    }
}
